package ru.mip.testserver.models;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import ru.mip.testserver.dto.UserAuthDto;
import ru.mip.testserver.dto.UserDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long     id;

    @Column(unique = true)
    private String login;
    private String password;

    @Column(unique = true)
    private String keyCard;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<UserGroup> userGroups = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.TRUE)
    @OneToMany(mappedBy = "user",orphanRemoval = true)
    private List<Operation> operations;

    private Integer balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyCard() {
        return keyCard;
    }

    public void setKeyCard(String keyCard) {
        this.keyCard = keyCard;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public UserDto toDto() {
        UserDto dto = new UserDto();
        dto.setId(id);
        dto.setBalance(balance);
        dto.setKeyCard(keyCard);
        return dto;
    }

    public UserAuthDto toAuthDto() {
        UserAuthDto dto = new UserAuthDto();
        dto.setId(id);
        dto.setLogin(login);
        dto.setGroups(
                userGroups.stream().map(UserGroup::getName).collect(Collectors.toList())
        );
        return dto;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }
}
