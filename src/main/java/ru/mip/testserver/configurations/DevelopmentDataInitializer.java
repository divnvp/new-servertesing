package ru.mip.testserver.configurations;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.mip.testserver.models.User;
import ru.mip.testserver.models.UserGroup;
import ru.mip.testserver.services.UserGroupService;
import ru.mip.testserver.services.UserService;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Component
public class DevelopmentDataInitializer {

    private final static long MAX_KEY_CARD = 9999999999L;

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());
    private final static Random R = new Random();

    @Autowired private UserService userService;
    @Autowired private UserGroupService userGroupService;

    @Transactional
    public void init() {

        UserGroup point = new UserGroup("POINT","Точка продаж");
        userGroupService.save(point);

        User adminUser = new User();
        adminUser.setLogin("point");
        adminUser.setPassword("point");
        adminUser.getUserGroups().add(point);
        userService.save(adminUser);

        log.info("Инициализируем тестовый набор данных");
        List<User> list = new LinkedList<>();
        int count = 1500;
        log.info("подготовка...");

        String[] keys = {"111", "222", "333", "444", "555", "666", "777", "888", "999",
            "4045858678", "0102650386", "2292015097"
        };

        for(int i=0; i < count; i++) {
            User user = new User();
            user.setBalance(R.nextInt(50000));
            user.setKeyCard( i < keys.length ? keys[i] : Long.toString(MAX_KEY_CARD - i));
            list.add(user);
        }
        log.info("сохранение");
        userService.saveAll(list);
        log.info("инициализация завершена.");
    }
}
