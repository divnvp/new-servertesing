package ru.mip.testserver.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DevelopmentConfiguration {

    @Bean(initMethod = "init")
    public DevelopmentDataInitializer initTestData() {return new DevelopmentDataInitializer();}


}
