package ru.mip.testserver.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.mip.testserver.models.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByLogin(String login);

    @Query("select u from User u where u.keyCard is not null")
    List<User> findAllWithKey();
}
