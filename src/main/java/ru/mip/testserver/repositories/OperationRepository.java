package ru.mip.testserver.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.mip.testserver.models.Operation;

public interface OperationRepository extends CrudRepository<Operation, Long> {

}
