package ru.mip.testserver.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.mip.testserver.models.UserGroup;

public interface UserGroupRepository extends CrudRepository<UserGroup, Long> {

}
