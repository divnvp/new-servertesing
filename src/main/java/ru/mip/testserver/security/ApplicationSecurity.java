package ru.mip.testserver.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import ru.mip.testserver.models.UserGroup;
import ru.mip.testserver.services.UserService;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = false)
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired private UserService userService;
    @Autowired private RESTAuthenticationEntryPoint authenticationEntryPoint;
    @Autowired private RESTAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired private RESTAuthenticationSuccessHandler restAuthenticationSuccessHandler;

//    POINT - доступ к /point/
//    TERM - доступ к /point/
//    TEACHER - доступ к /teacher/
//    ADMIN - полный доступ
//    MENU - редактирование и просмотр меню в /admin/
//    SOCIAL - редактирование и доступ к классам, обуч. и персоналу в /admin/
//    REQUEST - редактирование и просмотр заявок
//    STAT - доступ к статистике в /admin/
//    FIND - доступ к запросу владельцев карт
//    BIND - доступ к привязке карт
//    ACS_VIEW - просмотри статистики и информации о СКУД без редактирования

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.httpBasic().disable()
            .authorizeRequests()
                .anyRequest().authenticated().and()
//                .anyRequest().permitAll().and()
            .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
            .userDetailsService(userDetailsService())
            .formLogin()
                .loginPage("/auth")
                .permitAll()
                .usernameParameter("login")
                .passwordParameter("pwd")
                .successHandler(restAuthenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler).and()
            .csrf().disable()
            .logout()
                .permitAll();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return charSequence.toString().equals(s);
            }
        };
    }

    @Bean
    @Transactional(readOnly = true)
    protected UserDetailsService userDetailsService() {
        return username -> {
            log.info("LOGIN: {}",username);
            ru.mip.testserver.models.User user = userService.findUserByLogin(username);
            if(user!= null) {
                log.info("{} login as {}",username,user.getUserGroups());
                return new User(user.getLogin(), user.getPassword(), true, true, true, true,
                        AuthorityUtils.createAuthorityList(user.getUserGroups()
                                .stream().map(UserGroup::getName).toArray(String[]::new)));
            } else {
                log.info("{} access denied",username);
                throw new UsernameNotFoundException("could not find the user '"
                        + username + "'");
            }
        };
    }
}

