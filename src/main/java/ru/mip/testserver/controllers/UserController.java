package ru.mip.testserver.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mip.testserver.dto.OperationSyncDto;
import ru.mip.testserver.dto.UserDto;
import ru.mip.testserver.models.User;
import ru.mip.testserver.services.OperationService;
import ru.mip.testserver.services.UserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired private UserService userService;
    @Autowired private OperationService operationService;

    @GetMapping
    public Iterable<UserDto> index() {
        return userService.userList().stream().map(User::toDto).collect(Collectors.toList());
    }

    @PostMapping(path = "/operation")
    public List<Long> syncOperations(@RequestBody List<OperationSyncDto> operations) {
        return operationService.sync(operations);
    }
}
