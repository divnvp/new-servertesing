package ru.mip.testserver.dto;


public class UserDto {

    private Long     id;
    private String keyCard;
    private Integer balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyCard() {
        return keyCard;
    }

    public void setKeyCard(String keyCard) {
        this.keyCard = keyCard;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
