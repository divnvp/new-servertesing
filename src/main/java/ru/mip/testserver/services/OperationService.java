package ru.mip.testserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mip.testserver.dto.OperationSyncDto;
import ru.mip.testserver.exceptions.BadRequestException;
import ru.mip.testserver.exceptions.ResourceNotFoundException;
import ru.mip.testserver.models.Operation;
import ru.mip.testserver.models.User;
import ru.mip.testserver.repositories.OperationRepository;

import java.util.LinkedList;
import java.util.List;

@Service
public class OperationService {

    @Autowired private OperationRepository operationRepository;
    @Autowired private UserService userService;

    @Transactional
    public Operation save(Operation operation) {
        return operationRepository.save(operation);
    }

    public List<Long> sync(List<OperationSyncDto> operations) {
        List<Long> result = new LinkedList<>();
        for(OperationSyncDto dto: operations) {
            try {
                Long id;
                if((id = syncOperation(dto)) != null) {
                    result.add(id);
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Transactional
    public Long syncOperation(OperationSyncDto dto) {
        if(dto.getSum() == null) {
            throw new BadRequestException("Сумма не задана для операции с id: "+dto.getId());
        }
        if(dto.getDate() == null) {
            throw new BadRequestException("Дата не задана для операции с id: "+dto.getId());
        }
        User user = userService.getById(dto.getUserId()).orElseThrow( () ->
                new ResourceNotFoundException("Пользователь с id: "+dto.getUserId()+" не найден")
        );
        user.setBalance((int) (user.getBalance() - dto.getSum()));
        if(user.getBalance() < 0) {
            throw new BadRequestException("Применение операции дало отрицательный баланс: "+dto.getId());
        }
        userService.save(user);
        Operation operation = new Operation();
        operation.setDate(dto.getDate());
        operation.setLocalId(dto.getId());
        operation.setSum(dto.getSum());
        operation.setUser(user);
        save(operation);
        return dto.getId();
    }
}
