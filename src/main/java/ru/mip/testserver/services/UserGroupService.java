package ru.mip.testserver.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mip.testserver.models.UserGroup;
import ru.mip.testserver.repositories.UserGroupRepository;

@Service
public class UserGroupService {

    @Autowired private UserGroupRepository userGroupRepository;

    public UserGroup save(UserGroup userGroup) {
        return userGroupRepository.save(userGroup);
    }
}
