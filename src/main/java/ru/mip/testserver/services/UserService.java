package ru.mip.testserver.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mip.testserver.models.User;
import ru.mip.testserver.repositories.UserRepository;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired private UserRepository userRepository;

    public List<User> userList() {
        return userRepository.findAllWithKey();
    }

    public void saveAll(Iterable<User> users) {
        userRepository.saveAll(users);
    }

    public User findUserByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public Optional<User> getById(Long userId) {
        return userRepository.findById(userId);
    }
}
